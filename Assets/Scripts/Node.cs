using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    [SerializeField] private string name;
    [SerializeField] private int number;
    [SerializeField] private GameManager GameManagerScript;
    [SerializeField] private bool IsRaided;
    [SerializeField] private GameObject RaidSignal;
    [SerializeField] private NodesController NodesControllerScript;

    // Start is called before the first frame update
    void Start()
    {
        GameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        NodesControllerScript = GameObject.Find("Nodes").GetComponent<NodesController>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetIsRaided(bool value)
    {
        this.IsRaided = value;
        RaidSignal.SetActive(value);
    }
    public bool GetIsRaided()
    {
        return this.IsRaided;
    }
    
    public void setNode(string name, Mesh MeshName)
    {
        MeshFilter mesh = gameObject.GetComponent<MeshFilter>();
        mesh.sharedMesh = MeshName;
        this.name = name;
    }
    public int GetNumber()
    {
        return this.number;
    }
    public string GetNodeName()
    {
        return this.name;
    }

    private void OnMouseDown()
    {
        if (GameManagerScript.GetCanRaid())
        {

            NodesControllerScript.ResetRaid();
            SetIsRaided(true);
            GameManagerScript.SetRaidDisplay(false);
            GameManagerScript.SetCanRaid(false);
            GameManagerScript.SetBuildMenu(true);
            GameManagerScript.UpdateButton();
            GameManagerScript.CheckCanConvert();
            GameManagerScript.SetEndTurnButton(true);
        }
    }

}
