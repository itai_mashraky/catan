using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : MonoBehaviour
{

    [SerializeField] private List<Node> Nodes; // list of connected nodes
    [SerializeField] private bool IsEmpty; // is the point empty
    [SerializeField] private bool CanBuild = true, isStart = true, IsCastle = false;
    [SerializeField] private float Chance, counter, CounterReset;
    [SerializeField] private List<Road> RoadsNearPoint;
    [SerializeField] private int indexX, indexY;
    [SerializeField] private GameObject Indicator;
    [SerializeField] private int OwnerID;
    [SerializeField] private Mesh[] meshes;
    [SerializeField] private GameManager GameManagerScript;


    // Start is called before the first frame update
    void Start()
    {
        GameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        OwnerID = -1;
        IsEmpty = true;
        CanBuild = true;
        counter = CounterReset;

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.transform.CompareTag("Node"))
        {
            if (!Nodes.Contains(collision.transform.GetComponent<Node>()))
            {
                Nodes.Add(collision.transform.GetComponent<Node>());

            }
        }
        else if (collision.transform.CompareTag("Road"))
        {
            if (!RoadsNearPoint.Contains(collision.transform.GetComponent<Road>()))
            {
                RoadsNearPoint.Add(collision.transform.GetComponent<Road>());
            }
        }

    }

    public void SetIndex(int x, int y)
    {
        indexX = x;
        indexY = y;
    }

    public bool GetCanBuild()
    {
        return CanBuild;
    }

    public bool GetIsEmpty()
    {
        return IsEmpty;
    }

    public void SetCanBuild(bool value)
    {
        CanBuild = value;
    }

    public GameObject GetIndicator()
    {
        return Indicator;
    }

    private void OnMouseDown()
    {

        if (Indicator.activeSelf && IsEmpty)
        {
            IsEmpty = false;
            OwnerID = GameManagerScript.GetActivePlayer();
            Indicator.GetComponent<Renderer>().material.color = GameManagerScript.GetPlayers()[OwnerID].GetPlayerColor();

            GameManagerScript.GetPlayers()[OwnerID].SetVillageCount(GameManagerScript.GetPlayers()[OwnerID].GetVillageCount() + 1);

            Indicator.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            SetToVillage();
            GameManagerScript.DisableBuildOnNear(gameObject);
            GameManagerScript.ClearIndicator();
            if (GameManagerScript.GetIsStart())
            {
                if (GameManagerScript.GetPlayers()[GameManagerScript.GetActivePlayer()].GetVillageCount() == 1)
                {
                    for (int i = 0; i < Nodes.Count; i++)
                    {
                        GameManagerScript.GetPlayers()[GameManagerScript.GetActivePlayer()].SetResource(Nodes[i].GetNodeName(), 1);

                    }
                }
                GameManagerScript.EndTurn();
            }
            else
            {
                GameManagerScript.GetPlayers()[GameManagerScript.GetActivePlayer()].SetResource("Wood", -1);
                GameManagerScript.GetPlayers()[GameManagerScript.GetActivePlayer()].SetResource("Stone", -1);
                GameManagerScript.GetPlayers()[GameManagerScript.GetActivePlayer()].SetResource("Wheet", -1);
                GameManagerScript.GetPlayers()[GameManagerScript.GetActivePlayer()].SetResource("Wool", -1);
            }
        }
        else if (Indicator.activeSelf && GameManagerScript.GetCanUpgrade() && GameManagerScript.GetActivePlayer() == this.OwnerID)
        {
            Color tempColor = Indicator.GetComponent<Renderer>().material.color;
            GameManagerScript.GetPlayers()[OwnerID].SetVillageCount(GameManagerScript.GetPlayers()[OwnerID].GetVillageCount() - 1);
            GameManagerScript.GetPlayers()[OwnerID].SetTownCount(GameManagerScript.GetPlayers()[OwnerID].GetTownCount() + 1);
            tempColor.a = 1;
            Indicator.GetComponent<Renderer>().material.color = tempColor;
            IsCastle = true;
            GameManagerScript.SetCanUpgrade(false);
            GameManagerScript.ClearAfterUpgrade();
            GameManagerScript.GetPlayers()[GameManagerScript.GetActivePlayer()].SetResource("Wheet", -2);
            GameManagerScript.GetPlayers()[GameManagerScript.GetActivePlayer()].SetResource("Iron", -3);

        }
        if (!GameManagerScript.GetIsStart() && Indicator.activeSelf)
        {
            GameManagerScript.UpdateButton();
            GameManagerScript.CheckCanConvert();
        }
    }

    public void SetToVillage()
    {
        Indicator.GetComponent<MeshFilter>().sharedMesh = meshes[1];
        Color tempColor = Indicator.GetComponent<Renderer>().material.color;
        tempColor.a = 1f;
        Indicator.GetComponent<Renderer>().material.color = tempColor;

    }

    public int GetOwner_ID()
    {
        return OwnerID;
    }

    public List<Road> GetRoads()
    {
        return this.RoadsNearPoint;
    }

    public bool GetIsTown()
    {
        return IsCastle;
    }

    public void BuildCastle()
    {
        Indicator.GetComponent<MeshFilter>().sharedMesh = meshes[2];
        Color tempColor = Indicator.GetComponent<Renderer>().material.color;
        tempColor.a = 0.3f;
        Indicator.GetComponent<Renderer>().material.color = tempColor;
    }

    public Node HasNode(int number)
    {
        for (int i = 0; i < Nodes.Count; i++)
        {
            if (number == Nodes[i].GetNumber() && !Nodes[i].GetIsRaided())
            {
                return Nodes[i];
            }
        }
        return null;
    }
}
