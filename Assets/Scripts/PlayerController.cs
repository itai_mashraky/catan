using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private int[] ResourceCount;
    [SerializeField] private Resource Wheet; // 0
    [SerializeField] private Resource Wood; // 1
    [SerializeField] private Resource Wool; // 2
    [SerializeField] private Resource Iron; // 3
    [SerializeField] private Resource Stone; // 4
    [SerializeField] private List<Point> OwnedPoints;
    [SerializeField] private int Player_ID = 0, VillageCount, RoadCount, TownCount;
    [SerializeField] private string PlayerName;
    [SerializeField] private Color PlayerColor;


    [SerializeField] private GameManager GameManagerScript;

    // Start is called before the first frame update
    void Start()
    {

        GameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();

        ResourceCount = new int[5];
        for (int i = 0; i < ResourceCount.Length; i++)
        {
            ResourceCount[i] = 0;
        }
        MeshFilter mesh = gameObject.GetComponent<MeshFilter>();

    }

    // Update is called once per frame
    void Update()
    {
        if ((VillageCount + TownCount * 2) >= 10)
        {
            GameManagerScript.EndGame(this.PlayerName);
        }
    }

    public string GetPlayerName()
    {
        return this.PlayerName;
    }

    public void SetResource(string ResourceName, int value)
    {
        if (ResourceName.Equals("Wheet"))
        {
            ResourceCount[0] += value;
        }
        else if (ResourceName.Equals("Wood"))
        {
            ResourceCount[1] += value;

        }
        else if (ResourceName.Equals("Wool"))
        {
            ResourceCount[2] += value;
        }
        else if (ResourceName.Equals("Iron"))
        {
            ResourceCount[3] += value;
        }
        else if (ResourceName.Equals("Stone"))
        {
            ResourceCount[4] += value;
        }
        if (!GameManagerScript.GetIsStart())
        {
            GameManagerScript.FillPlayerDisplay();
        }
    }
    public Color GetPlayerColor()
    {
        return this.PlayerColor;
    }
    public int GetVillageCount()
    {
        return this.VillageCount;
    }
    public int GetRoadCount()
    {
        return this.RoadCount;
    }
    public int GetTownCount()
    {
        return this.TownCount;
    }
    public void SetVillageCount(int value)
    {
        this.VillageCount = value;
    }
    public void SetRoadCount(int value)
    {
        this.RoadCount = value;
    }
    public void SetTownCount(int value)
    {
        this.TownCount = value;
    }
    public int[] GetResourceAmount()
    {
        return this.ResourceCount;
    }
    public int GetPoints()
    {
        return VillageCount + (TownCount * 2);
    }

}
